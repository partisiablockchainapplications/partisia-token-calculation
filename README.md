Reference account - https://dashboard.partisiablockchain.com/info/account/00dae0d98dbbe72eb56a8efbcc957d08dfa817dfbe
Total tokens = Inuse+Unused

InUse = #Staked+#PendingUnused+#InTransit
#Staked - stakedTokens+sum(delegatedStakesToOthers.value)
#PendingUnused - sum(pendingUnstakes)+sum(pendingRetractedDelegatedStakes.value)
#InTransit = #InTransitTransfer + #InTransitDelegated
#inTransitTransfer - sum(storedPendingtransfers) if coinIndex= -1 and addTokensOrCoinsIfTransferSuccessful=false
#inTransitDelegated - sum(storedPendingStakeDelegations) if delegation type is called DELEGATED_STAKES or RETRACT_DELEGATED_STAKES 

Unused = #InSchedule+#Transferable
 #InSchedule - sum(vestingAccounts.tokens - vestingAccount.releasedtokens)
 #Transferable - "mpcTokens"

