import Big from 'big.js'
import accountState from './accountState.json'
import { IAccountState } from './interface'

// Reference account - https://dashboard.partisiablockchain.com/info/account/00dae0d98dbbe72eb56a8efbcc957d08dfa817dfbe
export const calcTotalMPC = (accountState?: IAccountState) => {
  if (!accountState) {
    return '0'
  }
  // ////////
  /* Inuse */
  // ////////

  // #Staked - stakedTokens+sum(delegatedStakesToOthers.value)
  const Staked = new Big(accountState.stakedTokens).plus(accountState.delegatedStakesToOthers.reduce((pv, cv, idx) => pv.plus(cv.value), new Big(0)))
  // #PendingUnused - sum(pendingUnstakes)+sum(pendingRetractedDelegatedStakes.value)
  const PendingUnused = accountState.pendingUnstakes
    .reduce((pv, cv, idx) => pv.plus(cv.value), new Big(0))
    .plus(accountState.pendingRetractedDelegatedStakes.reduce((pv, cv, idx) => pv.plus(cv.value), new Big(0)))

  // #inTransitTransfer - sum(storedPendingtransfers) if coinIndex= -1 and addTokensOrCoinsIfTransferSuccessful=false
  const inTransitTransfer = accountState.storedPendingTransfers
    .filter((v) => v.value.coinIndex === -1 && v.value.addTokensOrCoinsIfTransferSuccessful === false)
    .reduce((pv, cv, idx) => pv.plus(cv.value.amount), new Big(0))

  // #inTransitDelegated - sum(storedPendingStakeDelegations) if delegation type is called DELEGATED_STAKES or RETRACT_DELEGATED_STAKES
  const inTransitDelegated = accountState.storedPendingStakeDelegations
    .filter((d) => ['DELEGATED_STAKES', 'RETRACT_DELEGATED_STAKES'].includes(d.value.delegationType))
    .reduce((pv, cv, idx) => pv.plus(cv.value.amount), new Big(0))

  // #InTransit = #InTransitTransfer + #InTransitDelegated
  const InTransit = inTransitTransfer.plus(inTransitDelegated)

  // InUse = #Staked+#PendingUnused+#InTransit
  const InUse = Staked.plus(PendingUnused).plus(InTransit)

  // /////////
  /* Unused */
  // /////////
  //  #InSchedule - sum(vestingAccounts.tokens - vestingAccount.releasedtokens)
  const InSchedule = accountState.vestingAccounts
    .reduce((pv, cv, idx) => pv.plus(cv.tokens), new Big(0))
    .minus(accountState.vestingAccounts.reduce((pv, cv, idx) => pv.plus(cv.releasedTokens), new Big(0)))

  //  #Transferable - "mpcTokens"
  const Transferable = accountState.mpcTokens

  // Unused = #InSchedule+#Transferable
  const Unused = InSchedule.plus(Transferable)

  // Total tokens = Inuse+Unused
  const TotalTokens = InUse.plus(Unused)
  return TotalTokens.toFixed(0)
}
// ;(() => {
//   // const accountState =
//   const res = calcTotalMPC(accountState)
//   console.log(res)
//   // result is 4448143429
// })()
