export interface IAccountState {
  accountCoins: AccountCoin[]
  delegatedStakesFromOthers: DelegatedStakesFromOther[]
  delegatedStakesToOthers: DelegatedStakesToOther[]
  mpcTokens: string
  pendingRetractedDelegatedStakes: DelegatedStakesToOther[]
  pendingUnstakes: DelegatedStakesToOther[]
  spentFreeTransactions: SpentFreeTransactions
  stakeable: boolean
  stakedToContract: DelegatedStakesToOther[]
  stakedTokens: string
  storedPendingStakeDelegations: StoredPendingStakeDelegation[]
  storedPendingTransfers: StoredPendingTransfer[]
  vestingAccounts: VestingAccount[]
}

export interface AccountCoin {
  balance: string
}

export interface DelegatedStakesFromOther {
  key: string
  value: DelegatedStakesFromOtherValue
}

export interface DelegatedStakesFromOtherValue {
  acceptedDelegatedStakes: string
  pendingDelegatedStakes: string
}

export interface DelegatedStakesToOther {
  key: string
  value: string
}

export interface SpentFreeTransactions {
  epoch: string
  spentFreeTransactions: string
}

export interface StoredPendingStakeDelegation {
  key: string
  value: StoredPendingStakeDelegationValue
}

export interface StoredPendingStakeDelegationValue {
  amount: string
  counterPart: string
  delegationType: string
}

export interface StoredPendingTransfer {
  key: string
  value: StoredPendingTransferValue
}

export interface StoredPendingTransferValue {
  addTokensOrCoinsIfTransferSuccessful: boolean
  amount: string
  coinIndex: number
}

export interface VestingAccount {
  releaseDuration: string
  releaseInterval: string
  releasedTokens: string
  tokenGenerationEvent: string
  tokens: string
}
